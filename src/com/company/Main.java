package com.company;

import com.company.enums.Commands;
import com.company.models.Transaction;
import com.company.singleton.FruitLazySingleton;
import com.company.utils.InputParser;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("Welcome to Program!");

        FruitTrader fruitTrader = new FruitTrader();

        InputParser inputParser = new InputParser();
        Commands cmd= null;
        String fruitName;
        int quantity,price;

        while(cmd != Commands.QUIT){
            // input
            System.out.print("$ ");

            // will handle input parsing and validation
            try {
                inputParser.readCommand();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }
            cmd = inputParser.getCommands();

            switch ( cmd ) {
                case BUY:
                    fruitName = inputParser.getFruitName();
                    price = inputParser.getPrice();
                    quantity = inputParser.getQuantity();
                    fruitTrader.buy(new Transaction(quantity,price, FruitLazySingleton.getFruit(fruitName)));
                    System.out.println("BOUGHT "+quantity + "KG "
                            + fruitName + " AT " + price + " RUPEES/KG");
                    break;

                case SELL:
                    fruitName = inputParser.getFruitName();
                    price = inputParser.getPrice();
                    quantity = inputParser.getQuantity();

                    try {
                        fruitTrader.sell(new Transaction(quantity,price, FruitLazySingleton.getFruit(fruitName)));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    System.out.println("SOLD "+quantity + "KG "
                            + fruitName + " AT " + price + " RUPEES/KG");
                    break;

                case PROFIT:
                    System.out.println(fruitTrader.getProfit());
                    break;
            }

        }
        System.out.println("Exit From Program!");
    }
}
