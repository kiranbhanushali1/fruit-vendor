package com.company.strategy;

import com.company.models.Stock;
import com.company.models.Transaction;

import java.util.List;
import java.util.ListIterator;

public class FifoSelling implements SellingStrategy {
    @Override
    public int sell(Stock stock, Transaction toSell) throws Exception {
        List<Transaction> fruitTransactionList = stock.getTransactionList(toSell.getFruit());

        if( fruitTransactionList == null ) {
            throw new Exception("Stock is not available");
        }

        int soldQuantity = 0 , soldPrice  = 0;

        ListIterator<Transaction> iter = fruitTransactionList.listIterator();

        while(iter.hasNext()){
            var transaction = iter.next();
            if( soldQuantity + transaction.getWeight()  >= toSell.getWeight() ){
                int remaining = toSell.getWeight() - soldQuantity ;
                // set remaining weight
                transaction.setWeight( transaction.getWeight() -remaining);
                soldPrice += remaining * transaction.getPrice();
                soldQuantity += remaining ;
                break;
            }
            soldQuantity += transaction.getWeight();
            soldPrice += transaction.getPrice() * transaction.getWeight();
            iter.remove();
        }

        if( soldQuantity  !=  toSell.getWeight()){
            throw new Exception("Stock is not available");
        }

        return ( toSell.getPrice()* toSell.getWeight() - soldPrice );
    }
}
