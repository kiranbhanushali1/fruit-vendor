package com.company.strategy;

import com.company.models.Stock;
import com.company.models.Transaction;

public interface SellingStrategy {
    int sell(Stock stock , Transaction toSell) throws Exception;
}
