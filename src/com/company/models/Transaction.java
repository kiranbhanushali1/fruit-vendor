package com.company.models;

import com.company.models.Fruit;

public class Transaction {
    private int weight;
    private int price;
    private Fruit fruit;

    public Transaction(int weight, int price, Fruit fruit) {
        this.weight = weight;
        this.price = price;
        this.fruit = fruit;
    }

    public int getWeight() {
        return weight;
    }

    public Fruit getFruit() {
        return fruit;
    }

    public void setFruit(Fruit fruit) {
        this.fruit = fruit;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
