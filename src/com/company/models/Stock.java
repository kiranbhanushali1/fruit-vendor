package com.company.models;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Stock {
    Map<Fruit,List<Transaction>>  stock;
    public Stock() {
        stock = new HashMap<>();
    }

    public void add(Transaction toAdd){
        List<Transaction> fruitStock = stock.computeIfAbsent(toAdd.getFruit(), k -> new LinkedList<>());

        fruitStock.add(toAdd);
    }

    public List<Transaction> getTransactionList(Fruit fruit){
        return stock.get(fruit);
    }
}


