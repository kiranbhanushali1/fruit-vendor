package com.company.utils;

import com.company.enums.Commands;

import java.util.Scanner;

public class InputParser {

    private Commands commands;
    private   String fruitName;
    private int price  , quantity;

    public Commands getCommands() {
        return commands;
    }

    public String getFruitName() {
        return fruitName;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void readCommand() throws Exception {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        String[] tokens = line.split(" ");

        if( tokens.length == 0 ) {
            throw new Exception("Invalid Input");
        }
        else if( tokens.length == 1 ){
            // EXIT or PROFIT Command
            commands = Commands.lookupCommand(tokens[0]);
        }else if( tokens.length == 4 ) {
            commands = Commands.lookupCommand(tokens[0]);
            try{
                fruitName = tokens[1];
                price = Integer.parseInt(tokens[2]);
                quantity = Integer.parseInt(tokens[3]);
            }catch (NumberFormatException nf){
                throw new Exception("Price and Quantiy Should be Integer");
            }
        }else {
            throw new Exception("Invalid Input");
        }
    }
}
