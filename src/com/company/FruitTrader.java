package com.company;


import com.company.models.Stock;
import com.company.models.Transaction;
import com.company.strategy.FifoSelling;
import com.company.strategy.SellingStrategy;

public class FruitTrader {
    Stock stock;
    int grossProfit;// initially will be zero
    SellingStrategy sellingStrategy;

    public FruitTrader(){
        stock = new Stock();
        grossProfit = 0 ;
        sellingStrategy = new FifoSelling();
    }

    public void buy(Transaction toBuy){
        stock.add(toBuy);
    }

    public void sell(Transaction toSell) throws Exception{
        grossProfit+= sellingStrategy.sell(stock,toSell);
    }

    public int getProfit(){
        return grossProfit;
    }

}
