package com.company.enums;

public enum Commands{
    BUY , SELL , PROFIT , QUIT;

    public static Commands lookupCommand(final String type) throws Exception{
        for( Commands c : Commands.values()){
            if( c.name().equals(type)){
                // got enum return it
                return c;
            }
        }
        // throw error
        throw new Exception("Invalid Commnad!");
    }
}

